<?php

/**
 * @file
 * Referer Link Bean plugin class.
 *
 * @todo Any other settings to allow?
 *   - link target attribute could be very useful
 *   - class attribute (though block_class.module already provides that).
 *   - the referer pattern could work either way; whitelist or blacklist.
 */


class RefererLinkBean extends BeanPlugin {

  /**
   * Declares default block settings.
   */
  public function values() {
    $values = array(
      'referer_link_text' => '',
      'referer_pattern' => '',
      'show_fallback_link' => 0,
      'fallback_link_text' => '',
      'fallback_link_url' => '',
    );
    return array_merge(parent::values(), $values);
  }


  /**
   * Builds extra settings for the block edit form.
   *
   * @todo Add #description properties for all elements.
   *
   * @todo fieldsets, or containers + #states might help here.
   *
   * @todo add validation - if show_fallback_link is checked, we need the other
   *   two fallback link fields!
   */
  public function form($bean, $form, &$form_state) {
    $form['referer_link_text'] = array(
      '#type' => 'textfield',
      '#title' => t('Referer link text'),
      '#default_value' => check_plain($bean->referer_link_text),
    );

    $form['referer_pattern'] = array(
      '#type' => 'textfield',
      '#title' => t('Referer URL pattern'),
      '#default_value' => check_plain($bean->referer_pattern),
    );

    $form['show_fallback_link'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show a fallback link'),
      '#default_value' => $bean->show_fallback_link,
      '#description' => t('Show a fallback link, if the HTTP Referer does not match the pattern.'),
    );

    $form['fallback_link_text'] = array(
      '#type' => 'textfield',
      '#title' => t('Fallback link text'),
      '#default_value' => check_plain($bean->fallback_link_text),
    );
    $form['fallback_link_url'] = array(
      '#type' => 'textfield',
      '#title' => t('Fallback link URL'),
      '#default_value' => check_plain($bean->fallback_link_url),
    );

    return $form;
  }


  /**
   * Displays the bean.
   */
  public function view($bean, $content, $view_mode = 'default', $langcode = NULL) {

    // Output the fallback link if needed.
    if ($bean->show_fallback_link) {
      $options = array(
        'attributes' => array(
          'class' => 'referer-link-fallback',
          'data-referer-link-delta' => $bean->delta,
        ),
      );
      $content['#markup'] = l(
        check_plain($bean->fallback_link_text),
        check_plain($bean->fallback_link_url),
        $options
        );
    }
    else {
      $content['#markup'] = '<span data-referer-link-delta="' . $bean->delta . '"</span>';
    }

    // Attach behaviour javascript.
    $content['#attached']['js'] = array(
      drupal_get_path('module', 'refererlinkblock') .'/js/refererlinkblock.js' => array(
        'type' => 'file'
      ),
    );

    // Prepare settings for each bean instance, keyed by bean delta.
    $settings = array(
      $bean->delta => array(
        'refererLinkText'  => check_plain($bean->referer_link_text),
        'refererPattern'   => check_plain($bean->referer_pattern),
        'showFallbackLink' => $bean->show_fallback_link,
        'fallbackLinkText' => check_plain($bean->fallback_link_text),
        'fallbackLinkUrl'  => check_plain($bean->fallback_link_url),
      ),
    );

    // Attach JS settings for this bean instance.
    $content['#attached']['js'][] = array(
      'data' => array('refererLink' => $settings),
      'type' => 'setting',
    );

    return $content;
  }

}