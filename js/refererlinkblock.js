/**
 * @file
 */
(function ($) {

/**
 * Behavior for Referer Link Bean.
 */
Drupal.behaviors.refererLinkBean = {
  attach: function (context, settings) {
    $('.bean-referer-link', context).once('referer-link', function() {

      // Get the settings for this bean instance.
      var beanDelta = $('[data-referer-link-delta]', this).first().attr('data-referer-link-delta');
      var beanSettings = settings.refererLink[beanDelta];

      // If the HTTP referer is available, update the link as necessary.
      if ('referrer' in document && document.referrer.length > 0) {
        if (beanSettings.refererPattern.length > 0) {
          var pattern = new RegExp(beanSettings.refererPattern);
          if (pattern.test(document.referrer)) {
            $('[data-referer-link-delta]', this).replaceWith(Drupal.theme('refererLink', beanSettings));
          }
        }
        else {
          // Referrer available, no pattern match needed, just update the link.
          $('[data-referer-link-delta]', this).replaceWith(Drupal.theme('refererLink', beanSettings));
        }
      }

      // If, after the preceeding logic, there still isn't a link provided by
      // this module, then delete the entire block (or pane).
      if (!$('a[class^="referer-link"]', this).length) {
        this.remove();
      }
    });
  }
};

/**
 * Make a link to the referrer.
 */
Drupal.theme.prototype.refererLink = function(beanSettings) {
  return '<a class="referer-link" href="' + document.referrer + '">' + beanSettings.refererLinkText + '</a>';
}

})(jQuery);
